import json
from discord.ext import commands
from requests_futures.sessions import FuturesSession

from .base import BasePlugin


DEFAULT_REPLY_NO_SUB = "I can't meme about nothing!"


# All plugins should inherit form BasePlugin
class MeMePlugin(BasePlugin):
    # If you want to use sub commands you must use this decorator to create a group
    @commands.group(description="Get a dank meme: noris")
    async def meme(self, context):
        # Make sure to safely fail this command out!
        if context.invoked_subcommand is None:
            await context.send(DEFAULT_REPLY_NO_SUB)

    # Use the previous method as a decorator for sub commands
    @meme.command(description="Chuck Noris Quote",
                  brief="Chuck Noris Quote")
    async def noris(self, context):
        # Opens an AYSNC request session
        session = FuturesSession()
        try:
            # Request and produce the http request
            response = session.get('https://api.chucknorris.io/jokes/random').result()
        except:
            await context.send("The Chuck Noris service is down :(")
        # Deserialize the JSON response and return the result
        content = json.loads(response.content)
        await context.send(content['value'])

def setup(bot):
    bot.add_cog(MeMePlugin(bot))
