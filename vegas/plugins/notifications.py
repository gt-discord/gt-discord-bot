from discord.ext import commands
from discord.utils import get

from utils import strip_id
from utils import get_role
from .base import BasePlugin

DD_ROLE_FILTER = ("no", "notification", "deadlock",
                  "has read the pinned doc in arg",
                  "haven't read the rules",
                  "lamp'd", "straight a's!",
                  "birthday", "official stream commentator", "(dd)", )

ROLE_FILTER = ("notification", "she/her", "he/him", "they/them", "(all)", )


class NotificationPlugin(BasePlugin):
    @commands.group(description="Get added or removed to notification roles",
                    case_insensitive=True,
                    aliases=['n', 'noti',])
    async def notifications(self, context):
        if context.invoked_subcommand is None:
            await context.send('What do you want to do with notifications?')

    @notifications.command(name='give',
                    description='Give a notification role',
                    brief='Give a notification role (Disaster Director Only)',
                    aliases=['g',])
    async def give_role(self, context, username, *role_name):
        caller_roles = [role.name for role in context.message.author.roles]
        if 'Disaster Director' in caller_roles:
            if role_name:
                role_name = " ".join(role_name)
                role = await get_role(context, role_name)
                author = context.message.author
                user_id = await strip_id(context)
                user = get(context.message.guild.members, id=int(user_id))

                if any(word.lower() in role_name.lower() for word in DD_ROLE_FILTER):
                    try:
                        await user.add_roles(role, reason="Given roles by {}".format(author.mention))
                        await context.send("Added {} to {}".format(
                            role.name,
                            user.mention
                        ))
                    except Exception as e:
                        await context.send(e)
                        await context.send("I dont think I can do that...")
                else:
                    await context.send("Must be an approved role!")
        else:
            await context.send("Sorry you must be Disaster Director to use this command!")


    @notifications.command(name='remove',
                    description='Remove a notification role',
                    brief='Remove a notification role (Disaster Director Only)',
                    aliases=['r',])
    async def remove_role(self, context, username, *role_name):
        caller_roles = [role.name for role in context.message.author.roles]
        if 'Disaster Director' in caller_roles:
            if role_name:
                role_name = " ".join(role_name)
                role = await get_role(context, role_name)
                author = context.message.author
                user_id = await strip_id(context)
                user = get(context.message.guild.members, id=int(user_id))

                if any(word.lower() in role_name.lower() for word in DD_ROLE_FILTER):
                    try:
                        await user.remove_roles(role, reason="Role removed by {}".format(author.mention))
                        await context.send("Removed {} from {}.".format(
                            role.name,
                            user.mention
                        ))
                    except:
                        await context.send("I dont think I can do that...")
                else:
                    await context.send("Must be an approved role!")
        else:
            await context.send("Sorry you must be Disaster Director to use this command!")




    @notifications.command(name='join',
                    description='Join a notification role',
                    brief='Join a notification role')
    async def join_role(self, context, *role_name):
        if context.message.channel.id == 498298844392718346:
            return
        if role_name:
            role_name = " ".join(role_name)
            role = await get_role(context, role_name)
            author = context.message.author
            if any(word.lower() in role_name.lower() for word in ROLE_FILTER):
                await author.add_roles(role, reason="Self Role from VEGAS")
                await context.send("Added {} to the role {}".format(
                    author.mention,
                    role.name
                ))
            else:
                await context.send("Foolish humans... Make sure you are picking a role from list")


    @notifications.command(name='leave',
                    description='Leave a notification role',
                    brief='Leave a notification role',
                    )
    async def leave_role(self, context, *role_name):
        if role_name:
            role_name = " ".join(role_name)
            role = await get_role(context, role_name)
            author = context.message.author
            if any(word.lower() in role_name.lower() for word in ROLE_FILTER):
                await author.remove_roles(role, reason="Selfrole from VEGAS")
                await context.send("Removed {} from the role {}".format(
                    author.mention,
                    role.name
                ))
            else:
                await context.send("Foolish humans... Make sure you are picking a role from list")


    @notifications.command(name='list',
                    descriptions='List all of the current joinable roles',
                    brief='List all of the current joinable roles',
                    aliases=['l',])
    async def list(self, context):
        roles = context.message.guild.roles
        output = "```[Notification Roles]\n\n"
        for role in roles:
            if "notification" in role.name.lower():
                output += role.name + "\n"
        output += "```"
        await context.send(output)


def setup(bot):
    bot.add_cog(NotificationPlugin(bot))
